package zadanie3;

public class MainDiv {
    public static void main(String[] args) {
        int[] tab = new int[10000];
        for (int i = 0; i < 10000; i++) {
            tab[i] = i * 2;
        }

        System.out.println(findIndexOfIterative(501, tab));

    }

    public static int findIndexOfIterative(int toFind, int[] tablica) {

        int minRange = 0;
        int maxRange = tablica.length;


        int middleIndex = (minRange + ((maxRange - minRange) / 2)); // rozmiar tablicy / 2

        int counter = 0;
        while (middleIndex < tablica.length) {
            middleIndex = (minRange + ((maxRange - minRange) / 2));

            System.out.println("Sprawdzenie : " + ++counter);

            int wartosc = tablica[middleIndex];
            if (wartosc > toFind) {
                maxRange = middleIndex - 1; // skracam zakres o polowe (przesuwam min range)
            } else if (wartosc < toFind) {
                minRange = middleIndex + 1; // skracam zakres o polowe (przesuwam max range)
            } else if (wartosc == toFind) {
                return middleIndex; // element znaleziony
            }
            if (minRange == maxRange && wartosc != toFind) {
                return -1; // nie znalazlem wartosci i zwracam indeks ujemny
            }
        }

        //
        return -1;
    }
}
